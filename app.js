//Carregando módulos
  const express = require('express')
  const handlebars = require('express-handlebars')
  const bodyParser = require('body-parser')
  const app = express()
  const admin = require('./routes/admin')
  const usuarios = require('./routes/usuario')
  const path = require('path')
  const mongoose = require('mongoose')
  const session = require('express-session')
  const flash = require('connect-flash')
  require("./models/Postagem")
  const Postagem = mongoose.model("postagens")
  const passport = require('passport')
  require("./config/auth")(passport)

//Configurações
  //Session
    app.use(session({
      secret: "cursodenode",
      resave: true,
      saveUninitialized: true
    }))

    app.use(passport.initialize())
    app.use(passport.session())
    app.use(flash())
  //Middlaware
    app.use((req, res, next) => {
      res.locals.success_msg = req.flash("success_msg")
      res.locals.error_msg = req.flash("error_msg")
      res.locals.error = req.flash("error")
      res.locals.user = req.user || null;
      next()
    })
  //Body Parser
    app.use(bodyParser.urlencoded({extended: true}))
    app.use(bodyParser.json())
  //Handlebars
    app.engine('handlebars', handlebars({defaultLayout: 'main'}))
    app.set('view engine', 'handlebars')
  //Mongoose
    mongoose.Promise = global.Promise;
    mongoose.connect("mongodb://localhost/blogapp", {
      useNewUrlParser:true,
      useUnifiedTopology: true
    }).then(() => {
      console.log('Conectado ao Mongo');
    }).catch((Error) => {
      console.log('Error ao se conectar '+Error)
    })
  //Public
    app.use(express.static(path.join(__dirname,"public")))

  //Rotas
    app.get('/', (req, res) => {
      Postagem.find().populate("categoria").sort({data:"desc"}).then((postagens) => {
        res.render("index", {postagens: postagens})
      }).catch((err) => {
        req.flash("error_msg", "Houve um error ao renderizar postagens!")
        res.redirect("/404")
      })
    })

    app.get('/404', (req, res) => {
      res.send('Error 404!!')
    })
  
    //admin
    app.use('/admin', admin)
    app.use('/usuarios', usuarios)

//Outros
  const PORT = 8081
  app.listen(PORT, () => {
    console.log('Servidor Rodando....');
  })