const express = require('express')
const router = express.Router()
const mongoose = require("mongoose")
require("../models/Categoria")
const Categoria = mongoose.model("categorias")
require("../models/Postagem")
const Postagem = mongoose.model("postagens")
const {isAdmin} = require("../helpers/isAdmin")

router.get('/', isAdmin,(req, res) => {
  res.render("admin/index")
})

router.get('/posts', isAdmin, (req, res) => {
  res.send('Página de Posts')
})

//listagem
router.get('/categorias', isAdmin, (req, res) => {
  Categoria.find().sort({date:"desc"}).then((categorias) => {
    res.render("admin/categorias/categorias", {categorias: categorias})
  }).catch((err) => {
    req.flash("error_msg", "Não foi possível listar as categorias")
    res.redirect("/admin")
  })
})

//view de cadastro
router.get('/categorias/add', isAdmin, (req, res) => {
  res.render('admin/categorias/addcategorias')
})

// cadastrar categoria
router.post('/categorias/nova', isAdmin, (req, res) => {

  var erros = []

  if(!req.body.nome || typeof req.body.nome == undefined || req.body.nome == null) {
    erros.push({texto: "Nome inválido!"})
  }

  if(!req.body.slug || typeof req.body.slug == undefined || req.body.slug == null) {
    erros.push({texto: "Slug inválido!"})
  }

  if(req.body.nome.length < 2) {
    erros.push({texto: "Nome está com o valor muito pequeno"})
  }

  if(erros.length > 0){
    res.render("admin/categorias/addcategorias", {erros: erros})
  } else {
    const novaCategoria = {
      nome: req.body.nome,
      slug: req.body.slug
    }
    
    new Categoria(novaCategoria).save().then(() => {
      req.flash("success_msg", "Categoria cadastrada com sucesso")
      res.redirect("/admin/categorias");
    }).catch(() => {
      req.flash("error_msg", "Houve um error ao cadastrar categoria!")
      res.redirect("/admin");
    })
  }
})

//editar categorias
router.get("/categorias/edit/:id", isAdmin, (req,res) => {
  Categoria.findOne({_id:req.params.id}).then((categoria) => {
    res.render("admin/categorias/editcategoria", {categoria: categoria})
  }).catch((err) => {
    req.flash("error_msg", "Essa categoria não existe")
    req.redirect("/admin/categorias")
  })
})

//update categoria
router.post("/categorias/edit", isAdmin, (req, res) => {
  Categoria.findOne({_id: req.body.id}).then((categoria) => {

    categoria.nome = req.body.nome
    categoria.slug = req.body.slug

    categoria.save().then(() => {
      req.flash("success_msg", "Categoria editada com sucesso!")
      res.redirect("/admin/categorias")
    }).catch((err) => {
      req.flash("error_msg", "Error ao salvar edição da categoria!")
      res.redirect("/admin/categorias")
    })

  }).catch((err) => {
    req.flash("error_msg", "Aconteceu um error ao editr")
    res.redirect("/admin/categorias")
  })
})

//deletar categoria
router.post("/categorias/deletar", isAdmin, (req, res) => {
  Categoria.remove({_id: req.body.id}).then(() => {
    req.flash("success_msg", "Categoria deletada com sucesso!")
    res.redirect("/admin/categorias")
  }).catch((err) => {
    req.flash("error_msg", "Error ao deletar!!")
    res.redirect("/admin/categorias")
  })
})


//Postagens
router.get("/postagens", isAdmin, (req, res) => {
  Postagem.find().populate("categoria").sort({data:"desc"}).then((postagens) => {
    res.render("admin/postagens/postagens", {postagens: postagens})
  }).catch((err) => {
    req.flash("error_msg", "Houve um error ao listar!!")
    res.redirect("/admin")
  })
})

router.get("/postagem/add", isAdmin, (req, res) => {
  Categoria.find().then((categorias) => {
    res.render("admin/postagens/addpostagem", {categorias: categorias})
  }).catch((err) => {
    req.flash("error_msg", "Houve um error ao carregar formulário")
    res.redirect("/admin")
  })
})

router.post("/postagem/nova", isAdmin, (req,res) => {

  var erros = []

  if(req.body.categoria == "0") {
    erros.push({texto: "Categoria inválida, registre uma categoria"})
  }

  if(erros.length > 0) {
    res.render("admin/postagens/addpostagem", {erros: erros})
  } else {
    const novaPostagem = {
      titulo: req.body.titulo,
      descricao: req.body.descricao,
      conteudo: req.body.conteudo,
      categoria: req.body.categoria,
      slug: req.body.slug
    }

    new Postagem(novaPostagem).save().then(() => {
      req.flash("success_msg", "Postagem criada com sucesso!!");
      res.redirect("/admin/postagens")
    }).catch((err) => {
      req.flash("error_msg", "Houve um error durante salvar a postagem")
      res.redirect("/admin/postagens")
    })
  }

})

router.get("/postagem/edit/:id", isAdmin, (req, res) => {
  
  Postagem.findOne({_id: req.params.id}).then((postagem) => {

    Categoria.find().then((categorias) => {
      res.render("admin/postagens/editpostagem", {categorias: categorias, postagem: postagem})
    }).catch((err) => {
      req.flash("error_msg", "Houve um error ao listar as categorias")
      res.redirect("/admin/postagens")
    })
    
  }).catch((err) => {
    req.flash("error_msg", "Houve um error ao carregar formulário")
    res.redirect("/admin/postagens")
  })

})

router.post("/postagem/edit", isAdmin, (req, res) => {
  
  Postagem.findOne({_id: req.body.id}).then((postagem) => {

    postagem.titulo = req.body.titulo,
    postagem.slug = req.body.slug,
    postagem.descricao = req.body.descricao,
    postagem.conteudo = req.body.conteudo,
    postagem.categoria = req.body.categoria

    postagem.save().then(() => {
      req.flash("success_msg", "Postagem editada com sucesso")
      res.redirect("/admin/postagens")
    }).catch((err) => {
      req.flash("error_msg", "Error interno!")
      res.redirect("/admin/postagens")
    })

  }).catch((err) => {
    req.flash("error_msg", "Houve um error ao carregar formulário")
    res.redirect("/admin/postagens")
  })

})

router.get("/postagem/deletar/:id", isAdmin, (req, res) => {
  Postagem.remove({_id: req.params.id}).then(() => {
    req.flash("success_msg", "Deletado com sucesso!")
    res.redirect("/admin/postagens")
  }).catch((err) => {
    req.flash("error_msg", "Houve um error interno")
    res.redirect("/admin/postagens")
  })
})

module.exports = router